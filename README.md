
## My First Person Shooter game demo

A minimal, Design Pattern driven, First Person Shooter game and framework.
Written entirely in C++, using the OpenGL API for rendering, this is a basic
game framework, designed to be simple to use and easy to maintain.

Project done around the third semester of my Game Development degree, circa 2011.

A couple nice features about it:

- Loading and rendering of animated DOOM3 MD5 models.
- Support for programmable OpenGL shaders (GLSL).
- Reference counting (COM-style).
- FMOD-based sound system.
- Support for animated sprites and billboards.

NOTE: This project is quite old and I'm no longer updating it.
It was coded against Windows and the Visual Studio 2008 IDE, using
fixed-function OpenGL for most of the 2D and 3D rendering. Code is
also outdated, written in C++98-style using a few Microsoft extensions.

**An in-game screenshot:**

![First Person Shooter](https://bytebucket.org/glampert/fps-game/raw/ba752059a350c3e68307071844396ca8f09f4351/fps-game.png "First Person Shooter game")

----

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

