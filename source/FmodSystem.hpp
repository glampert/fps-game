
// ===============================================================================================================
// -*- C++ -*-
//
// FmodSystem.hpp - FMOD sound system helper classes and functions.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef FMOD_SYSTEM_HPP
#define FMOD_SYSTEM_HPP

/*
 * The FMOD library is copyright of Firelight Technologies Ltd.
 * These classes only provide a C++ interface to their API.
 */
#include <Fmod.hpp>
#include <Common.hpp>
#include <RefPtr.hpp>
#include <ReferenceCountable.hpp>

namespace FMOD
{

///
/// SoundSystem -- Initializes an terminates the FMOD sound system.
/// The SoundSystem class is a singleton.
///
class SoundSystem
{
public:

	/// Init the FMOD library.
	static bool Initialize(int mixRate, int maxSoftwareChannels, unsigned int Flags);

	/// Close the FMOD library and do a proper cleanup.
	static void Shutdown();

	/// Checks if the sound system is in a good state.
	static bool Good();

	/// Load a sound sample from a file. Will return a null pointer on failure.
	static class Sound * CreateSoundFromFile(const std::string & fileName);

private:

	static bool initalized;

	 SoundSystem() { }
	~SoundSystem() { }
};

///
/// Sound -- FMOD sound object. Represents a sound sample loaded from a file.
/// See the FMOD library documentation for further information.
///
class Sound
	: public ReferenceCountable
{
	friend class SoundSystem;

public:

	// Public Interface:
	void SetVolume(int volume);
	void SetVolumeAbsolute(int volume);

	bool IsPlaying() const;
	void Play();
	void Stop();

	bool Fail() const { return sample == 0; };
	int GetChannel() const { return sampleChannel; };
	FSOUND_SAMPLE * GetSample() const { return sample; };

	// ReferenceCountable Methods:
	virtual unsigned long AddRef() const;
	virtual unsigned long Release() const;
	virtual unsigned long ReferenceCount() const;

private:

	Sound() { }
	Sound(const std::string & fileName);

	~Sound();

	int sampleChannel;
	FSOUND_SAMPLE * sample;
};

///
/// Reference countable sound pointer type.
///
typedef RefPtr<Sound> SoundPtr;

}; // namespace FMOD {}

#endif // FMOD_SYSTEM_HPP
