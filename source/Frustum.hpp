
// ===============================================================================================================
// -*- C++ -*-
//
// Frustum.hpp - Frustum culling.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef FRUSTUM_HPP
#define FRUSTUM_HPP

#include <Common.hpp>
#include <Vector.hpp>
#include <Matrix4x4.hpp>

///
/// Frustum class to provide frustum culling checks
/// for common computational geometry objects.
/// \note Optimized with SSE intrinsics.
///
SSE_ALIGN(class) Frustum
{
public:

	enum { A, B, C, D };

	#if (ENABLE_SSE_INTRINSICS)
	union
	{
		struct
		{
			__m128 clippingPlanes[6]; ///< 6 Frustum planes
		};
		float p[6][4];
	};
	#else
	float p[6][4];
	#endif // ENABLE_SSE_INTRINSICS

public:

	Frustum() { /* Leave uninitialized */ }
	Frustum(const Matrix4x4 & matView, const Matrix4x4 & matProj);
	void ComputeClippingPlanes(const Matrix4x4 & matView, const Matrix4x4 & matProj);

	// Bounding Volume Frustum Testing:
	bool TestPoint(float x, float y, float z) const;
	bool TestSphere(float x, float y, float z, float radius) const;
	bool TestCube(float x, float y, float z, float size) const;
	bool TestAABB(const Vec3 & mins, const Vec3 & maxs) const;
};

#endif // FRUSTUM_HPP
