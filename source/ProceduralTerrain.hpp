
// ===============================================================================================================
// -*- C++ -*-
//
// ProceduralTerrain.hpp - A proceduraly generated terrain that is the main game level.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef PROCEDURAL_TERRAIN_HPP
#define PROCEDURAL_TERRAIN_HPP

#include <GameLevel.hpp>
#include <Texture.hpp>

///
/// ProceduralTerrain -- Implementation of the GameLevel interface.
/// This is one of the possible implementations for a game level and currently the only one available.
///
class ProceduralTerrain
	: public GameLevel
{
	friend class Renderer;

public:

	ProceduralTerrain(int size_x, int size_z, int scaleFactor, int noiseFactor,
	                  const Texture * tex, const std::string & levelName);

	virtual ~ProceduralTerrain();

	// GameLevel Methods:
	virtual float GetHeightAt(float x, float z) const;
	virtual float GetScale() const;
	virtual void GetExtents(int & max_x, int & max_z) const;
	virtual Type GetLevelType() const;
	virtual bool Fail() const;

private:

	const Texture * texture; ///< Texture to apply over the height field

	// Height field stuff:
	int sizeX;
	int sizeZ;
	int scale;
	unsigned int * indexArray;
	float (*vertexArray)[3];
	float (*texCoordArray)[2];
};

#endif // PROCEDURAL_TERRAIN_HPP
