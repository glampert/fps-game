
// ===============================================================================================================
// -*- C++ -*-
//
// FireEffect.hpp - GPU Fire effect.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef FIRE_FX_HPP
#define FIRE_FX_HPP

#include <Vector.hpp>
#include <Texture.hpp>
#include <ShaderProgram.hpp>

///
/// Billboarded GPU fire effect.
///
class FireEffect
{
public:

	/// Construct new fire effect.
	FireEffect(Texture * diffuseMap, Texture * distortionMap, Texture * opacityMap,
	           float sizeFactor, const Vec3 & position);

	/// Set the distortion amount.
	void SetDistortionAmount(const Vec3 & amt);

	/// Set the height attenuation.
	void SetHeightAttenuation(const Vec3 & att);

	/// Enable the effect.
	static void Enable();

	/// Draw the flame billboard. (Call between Enable()/Disable() !)
	void Render() const;

	/// Disable the effect (return to fixed functionality).
	static void Disable();

	/// Cleanup.
	~FireEffect();

private:

	// Shader program (shared)
	static ShaderProgram * fireShader;

	// Effect tunnig variables
	Vec3 distortionAmount;
	float heightAttenuationX;
	float heightAttenuationY;

	// Fire textures
	Texture * fireDistortion;
	Texture * fireOpacity;
	Texture * fireBase;

	float size; ///< Billboard scale
	Vec3 pos;   ///< Billboard world position (x,y,z)
};

#endif // FIRE_FX_HPP
