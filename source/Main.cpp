
// ===============================================================================================================
// -*- C++ -*-
//
// Main.cpp - Application entry point.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#include <Common.hpp>
#include <Game.hpp>

#ifdef _DEBUG

class Console
	: public LogListener
{
public:

	Console()
	{
		Log::Instance().AddListener(this);
	}

	void ReceiveMessage(const std::string & msg)
	{
		printf(msg.c_str());
	}

} debugConsole;

#else

#if defined (_MSC_VER) && (_MSC_VER > 1000)
#pragma comment (linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"") // Suppress the console window.
#endif

#endif // _DEBUG

// ===============================================================================================================

// The game instance.
static Game * myGame = 0;

static void Initialize()
{
	myGame = new FPS_GameDemo();
	myGame->Initialize();
}

static void Cleanup()
{
	// Release all references and free memory:
	myGame->Quit();
	delete myGame;
}

static void GLUT_DisplayCallback()
{
	myGame->UpdateAndRender();
}

static void GLUT_KeyboardCallback(unsigned char key, int x, int y)
{
	if (key == 27) // Escape key:
	{
		exit(0);
	}
	else if (key == 'r') // 'R' reloads the gun:
	{
		myGame->GetPlayer(0)->GetWeapon()->Reload();
	}
	else
	{
		return;
	}
}

static void GLUT_MouseCallback(int button, int state, int x, int y)
{
	if ((button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN))
	{
		myGame->GetPlayer(0)->GetWeapon()->Fire();
	}
}

// ===============================================================================================================

int main(/*int argc, char * argv[]*/)
{
	Initialize();
	atexit(Cleanup);

	// Setup GLUT function pointers:
	glutIdleFunc(GLUT_DisplayCallback);
	glutDisplayFunc(GLUT_DisplayCallback);
	glutKeyboardFunc(GLUT_KeyboardCallback);
	glutMouseFunc(GLUT_MouseCallback);

	glutMainLoop();
	exit(0);
}
