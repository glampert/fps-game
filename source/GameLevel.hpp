
// ===============================================================================================================
// -*- C++ -*-
//
// GameLevel.hpp - Base class describing a game level.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef GAME_LEVEL_HPP
#define GAME_LEVEL_HPP

#include <vector>
#include <Common.hpp>
#include <Vector.hpp>
#include <RefPtr.hpp>
#include <ReferenceCountable.hpp>

///
/// GameLevel -- Generic game level interface. Exposes basic game level routines.
///
class GameLevel
	: public ReferenceCountable
{
public:

	enum Type { PROCEDURAL_TERRAIN };

	/// Returns the level height at a givem X and Z pos.
	/// This is used to set the correct player Y position as he moves thru the level.
	virtual float GetHeightAt(float x, float z) const = 0;

	/// Get the map scale factor.
	virtual float GetScale() const = 0;

	/// Get the level maximum extents in the x, and z directions.
	virtual void GetExtents(int & max_x, int & max_z) const = 0;

	/// Get the game level type.
	virtual Type GetLevelType() const = 0;

	/// Checks if the level creation failed.
	virtual bool Fail() const = 0;

	// ReferenceCountable Methods:
	virtual unsigned long AddRef() const;
	virtual unsigned long Release() const;
	virtual unsigned long ReferenceCount() const;

	/// Get the level name.
	const std::string & GetName() const { return name; };

	/// Gives a random x and z position in the game map. Also sets the y to a proper height.
	void GetRandomPos(Vec3 & pos, int max_x, int max_z) const
	{
		pos.x =  RandCoord(max_x);
		pos.z = -RandCoord(max_z);
		pos.y =  this->GetHeightAt(pos.x, pos.z);
	}

public:

	enum ObjectType
	{
		FLAME_FIRE,
		ENEMY_INSTANCE,
		AMMO_BOXES,
		SKY_BOX
	};

	struct Object
	{
		void * objPointer;
		int type;
	};

	std::vector<Object> objects;

protected:

	virtual ~GameLevel();

	std::string name;
};

///
/// Reference countable GameLevel pointer type.
///
typedef RefPtr<GameLevel> GameLevelPtr;

// ===============================================================================================================

#define NUM_BILLBOARDS 100
#define NUM_AMMO_BOXES 20
#define NUM_ENEMIES    3

///
/// LevelBuilder -- Builds a game level.
/// The level builder is a singleton.
///
class LevelBuilder
{
public:

	/// "Build" a game level by populating it with objects, enemies and other scene stuff.
	static bool Build(GameLevel * level);

	/// Get the built game level.
	static GameLevel * GetGameLevel();

private:

	 LevelBuilder() { }
	~LevelBuilder() { }

	static GameLevel * myLevel;
};

#endif // GAME_LEVEL_HPP
