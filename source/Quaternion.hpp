
// ===============================================================================================================
// -*- C++ -*-
//
// Quaternion.hpp - Quaternion math.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef QUATERNION_HPP
#define QUATERNION_HPP

#include <Math.hpp>
#include <Vector.hpp>

///
/// Quaternion Class
///
class Quaternion
{
public:

	union
	{
		struct
		{
			float x;
			float y;
			float z;
			float w;
		};
		float element[4];
	};

	// Constructors:
	Quaternion();
	Quaternion(const Quaternion & quat);
	Quaternion(float X, float Y, float Z, float W);

	// Operators:
	Quaternion & operator = (float Val);
	Quaternion & operator = (const Quaternion & q);
	Quaternion   operator * (const Vec3 & v) const;
	Quaternion   operator * (const Quaternion & q) const;

	/// Computes the W component of the quaternion.
	void ComputeW();

	/// The absolute length of this quaternion.
	float Length() const;

	/// Normalize this quaternion. (Divide it by it's length).
	void Normalize();

	/// Rotates a given point using this quaternion.
	void RotatePoint(const Vec3 & in, Vec3 & out) const;

	/// Dot product between this quaternion and 'quat'.
	float DotProduct(const Quaternion & quat) const;

	/// Spherical Linear Interpolation.
	Quaternion Slerp(const Quaternion & quat, float t) const;
};

#endif // QUATERNION_HPP
