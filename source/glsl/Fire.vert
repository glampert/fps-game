
// ===============================================================================================================
// -*- VERTEX SHADER -*-
//
// Fire.vert - GLSL vertex shader for a billboarded fire effect.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

uniform float time;
const vec4 layerSpeed = vec4(0.68753, 0.52000, 0.75340, 1.0);

varying vec2 vTexCoord0;
varying vec2 vTexCoord1;
varying vec2 vTexCoord2;
varying vec2 vTexCoord3;

void main()
{
	// Output TexCoord0 directly
	vTexCoord0 = vec2(gl_MultiTexCoord0.x, gl_MultiTexCoord0.y);

	// Base texture coordinates plus scaled time
	vTexCoord1.x = gl_MultiTexCoord0.x;
	vTexCoord1.y = (gl_MultiTexCoord0.y) + layerSpeed.x * time;

	// Base texture coordinates plus scaled time
	vTexCoord2.x = gl_MultiTexCoord0.x;
	vTexCoord2.y = (gl_MultiTexCoord0.y) + layerSpeed.y * time;

	// Base texture coordinates plus scaled time
	vTexCoord3.x = gl_MultiTexCoord0.x;
	vTexCoord3.y = (gl_MultiTexCoord0.y) + layerSpeed.z * time;

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
