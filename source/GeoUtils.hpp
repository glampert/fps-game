
// ===============================================================================================================
// -*- C++ -*-
//
// GeoUtils.hpp - Geometry tools and utilities.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef GEO_UTILS_HPP
#define GEO_UTILS_HPP

#include <Common.hpp>
#include <Vector.hpp>

///
/// Basic vertex structure.
///
struct Vertex
{
	Vec3 position;
	Vec3 normal;
	float st[2];
};

///
/// Indexed triangle set.
///
struct Triangle
{
	unsigned short vertices[3];
};

/// Generates an OpenGL display list from the given vertices and triangle indices.
/// Use glDeleteLists() to destroy the returned list.
GLuint CreateDisplayList(const Vertex vertices[], int numVerts, const Triangle triangles[], int NumTris);

/// Fills the input vertices and triangle indices with data representing a 3D box.
/// you can then pass this data to OpenGL for direct drawing or use it to create a display list.
/// The box has vertices, normals and texture coordinates.
void Create3DBox(Vertex vertices[24], Triangle triangles[12], float width, float height, float depth);

///
/// Axis aligned bounding box
///
struct BoundingBox
{
	Vec3 min;
	Vec3 max;
};

/// Create an axis aligned bounding box from a set of points.
bool BoundingBoxFromPoints(const Vec3 * points, int numPoints, BoundingBox * box);

/// Test a axis aligned bounding box collision agains a point.
bool BoundingBoxCollision(const BoundingBox & box, const Vec3 & point);

/// Test a collision between two axis aligned bounding boxes.
bool BoundingBoxCollision(const BoundingBox & box1, const Vec3 & box1Pos, const BoundingBox & box2, const Vec3 & box2Pos);

#endif // GEO_UTILS_HPP
