
// ===============================================================================================================
// -*- C++ -*-
//
// ShaderProgram.hpp - GPU shader program interface.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef SHADER_PROGRAM_HPP
#define SHADER_PROGRAM_HPP

#include <vector>
#include <Common.hpp>
#include <RefPtr.hpp>
#include <ReferenceCountable.hpp>

///
/// Interface for a GPU shader program.
/// Supported types are the Fragment Shader (Pixel Shader)
/// and the Vertex Shader, currently implemented for the OpenGL rendering API.
/// Shader info logs are redirected to the framework Log system by default.
///
class ShaderProgram
	: public ReferenceCountable
{
public:

	/// Creates a new shader program from the specified source files.
	/// The shader type (i.e: Vertex/Fragment shader) is detected by checking the source file extension,
	/// which must be either ".vert" for vertex shaders or ".frag" for fragment shaders.
	static ShaderProgram * Create(const std::string * srcFiles, size_t numFiles);

	/// Enable the shader program.
	void Enable() const;

	/// Disable the shader program (return to fixed functionality).
	void Disable() const;

	// TODO: Add get/set methods for the different shader data types !!!

	/// Set a shader uniform variable.
	void SetUniform1i(const char * name, int i);

	/// Set a shader uniform variable.
	void SetUniform1f(const char * name, float f);

	/// Set a shader uniform vector[4].
	void SetUniform4f(const char * name, float vec[4]);

	// ReferenceCountable Methods:
	virtual unsigned long AddRef() const;
	virtual unsigned long Release() const;
	virtual unsigned long ReferenceCount() const;

protected:

	virtual ~ShaderProgram();

private:

	// This will prevent the class from being statically allocated.
	// The only way to create an instance of it now is via the ShaderProgram::Create() method.
	ShaderProgram(const std::string * srcFiles, size_t numFiles);

	// Disable copy/assignment.
	ShaderProgram(const ShaderProgram &);
	ShaderProgram & operator = (const ShaderProgram &);

	// Write the program info log to the default log output.
	void WriteInfoLog() const;

	GLuint programObj; ///< Shader program object for OpenGL
	std::vector<GLuint> shaders; ///< Vertex & Fragment shader instances in this shader program
};

#endif // SHADER_PROGRAM_HPP
