
// ===============================================================================================================
// -*- C++ -*-
//
// Player.hpp - Player class.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <Camera.hpp>
#include <Weapon.hpp>
#include <GameLevel.hpp>

///
/// Player -- Simple game player representation.
///
class Player
{
public:

	// Public Constants:
	static const float CAM_MOVE_SPEED;
	static const float CAM_ROTATE_SPEED;

	// Construction / Destruction:
	Player(int intialAmmo, const Vec3 & initialPos);
	~Player();

	/// Update the player states. Called every frame.
	void Update(const GameLevel * level, float elapsedTime);

	/// Draw the HUD and player weapon.
	void DisplayHUDAndWeapon();

	/// Checks if the initialization failed.
	bool Fail() const;

	// Data Access:
	Camera & GetCamera() { return camera; /* Editable */ }
	Weapon * GetWeapon() { return weapon; /* Editable */ }
	float GetAngle() const { return rAngle; }

private:

	/// Checks for keyboard input then update the camera. (System dependent)
	void CameraKeyboardInput();

	/// Checks for mouse input then update the camera. (System dependent)
	void CameraMouseInput();

private:

	Camera   camera;      ///< The player camera. I use the camera position as the player position in the world.
	float    pitchAmt;    ///< Amount we've looked up or down.
	float    rAngle;      ///< Angle of rotation for the player. This is used to orient enemies toward the player.
	Weapon * weapon;      ///< Player weapon object.
	Image  * hudTextArea; ///< HUD text area, currently a hardcoded image file.
};

#endif // PLAYER_HPP
