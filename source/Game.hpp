
// ===============================================================================================================
// -*- C++ -*-
//
// Game.hpp - Game class. Encapsulates the game logic. Demonstrates the use of the Template Method Design pattern.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef GAME_HPP
#define GAME_HPP

#include <Common.hpp>
#include <Renderer.hpp>
#include <FmodSystem.hpp>

#include <Math.hpp>
#include <Matrix4x4.hpp>
#include <FireEffect.hpp>

#include <Player.hpp>
#include <Enemy.hpp>
#include <Billboard.hpp>

#include <GameLevel.hpp>
#include <ProceduralTerrain.hpp>

///
/// Game -- Abstract class that encapsulates most of the game logic.
/// Demonstrates the use of the 'Template Method' Design pattern.
///
class Game
{
public:

	// Template Methods:

	/// Initialize the game.
	bool Initialize();

	/// Update the game timer and render one frame.
	void UpdateAndRender();

	/// Cleanup and exit.
	void Quit();

	// Public Interface:

	Game();
	virtual ~Game();

	/// Return the number of players.
	size_t GetNumPlayers() const { return numPlayers; };

	/// Get a player instance.
	Player * GetPlayer(size_t i) const { return players[i]; };

	/// Return the number of levels.
	size_t GetNumLevels() const { return numLevels; };

	/// Get a game level instance.
	GameLevel * GetGameLevel(size_t i) const { return levels[i]; };

protected:

	/*
	 * The following methods are implemented by a specific game
	 * and are called by the template methods of this class.
	 * Deriving classes are free to implement them in any way they see fit.
	 */

	virtual bool InitRenderingSystem() = 0;
	virtual bool InitSoundSystem() = 0;
	virtual bool LoadGameData() = 0;

	virtual void UpdateFrame(float elapsedTime) = 0;
	virtual void RenderFrame() = 0;

	virtual void TermRenderingSystem() = 0;
	virtual void TermSoundSystem() = 0;
	virtual void FreeGameData() = 0;

protected:

	size_t numPlayers;
	Player ** players;

	size_t numLevels;
	GameLevel ** levels;

	float currentTime, lastTime;
};

// ===============================================================================================================

///
/// Implements the Game interface.
///
class FPS_GameDemo
	: public Game
{
public:

	FPS_GameDemo();

protected:

	virtual bool InitRenderingSystem();
	virtual bool InitSoundSystem();
	virtual bool LoadGameData();

	virtual void UpdateFrame(float elapsedTime);
	virtual void RenderFrame();

	virtual void TermRenderingSystem();
	virtual void TermSoundSystem();
	virtual void FreeGameData();

private:

	FMOD::Sound * sndTrack;      ///< Background music
	FMOD::Sound * sndAmmoPickup; ///< Ammo pickup sound effect
	float prcLoaded;             ///< Percentage of the game loaded. From 0 to 1
	float elapsedTime;           ///< Game timer

	// Other game specific stuff:
	Texture * groundNmap;
	Texture * HellknightNmap;
	ShaderProgram * normalMapShader;
};

#endif // GAME_HPP
