
// ===============================================================================================================
// -*- C++ -*-
//
// Texture.hpp - Definition of a texture interface and an OpenGL texture classe.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <memory>
#include <string>

#include <Image.hpp>
#include <RefPtr.hpp>
#include <ReferenceCountable.hpp>

// ===============================================================================================================
// Texture class diagram:
//
//     +--------- (abs)         +-------------------- (abs)
//     |  Texture  | ---------> |  ReferenceCountable  |
//     +-----------+            +----------------------+
//           ^
//           |
//   +-----------------+
//   |  GL_2D_Texture  | ---------------> By using a virtual interface for the textures one can
//   +-----------------+                  change the real texture implementation easily to support other APIs.
//
// ===============================================================================================================

///
/// Texture -- Texture base class.
/// This is an abstract base class for more specialized OpenGL texture classes.
/// Texture objects can only be created by the Renderer class.
///
/// The Texture class is reference counted.
///
class Texture
	: public ReferenceCountable
{
public:

	enum
	{
		DEFAULT  = (1 << 0), ///< Default behaviour
		COMPRESS = (1 << 1)  ///< Use texture compression
	};

	typedef int TextureFlags;

	Texture();

	/// Checks if the texture creation failed.
	bool Fail() const;

	/// True if using the OpenGL standard coord system.
	bool StdCoordSystem() const;

	/// Get the name of the image file that originated the texture.
	const std::string & FileName() const;

	/// Binds the texture for rendering.
	virtual void Bind(unsigned int texUnit = 0) const = 0;

	// ReferenceCountable Methods:
	virtual unsigned long AddRef() const;
	virtual unsigned long Release() const;
	virtual unsigned long ReferenceCount() const;

protected:

	// Not allowed to be directly deleted.
	virtual ~Texture() { }

	TextureFlags flags;   ///< Texture compression flags
	std::string fileName; ///< File that originated the texture

	bool standardCoordSystem; ///< GL std coord system
	bool failBit; ///< Set if an error occurred during creation
};

///
/// Reference countable texture pointer type.
///
typedef RefPtr<Texture> TexturePtr;

// ===============================================================================================================

///
/// GL_2D_Texture -- OpenGL 2D texture object.
///
class GL_2D_Texture
	: public Texture
{
public:

	/// Construct from image object.
	GL_2D_Texture(const Image * img, TextureFlags imgFlags = Texture::DEFAULT);

	/// Binds a texture to a geometry.
	virtual void Bind(unsigned int texUnit = 0) const;

protected:

	GL_2D_Texture() { }
	virtual ~GL_2D_Texture();

	/// Create a texture 2D from an image.
	virtual bool Create(const Image * img, TextureFlags imgFlags);

	/// Return the corresponding format for a compressed image given image's internal format (pixel components).
	virtual GLint GetCompressionFormat(GLint components) const;

private:

	GLuint id; ///< The OpenGL texture id
};

#endif // TEXTURE_HPP
