
// ===============================================================================================================
// -*- C++ -*-
//
// Billboard.hpp - Definition of a billboard class.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef BILLBOARD_HPP
#define BILLBOARD_HPP

#include <Vector.hpp>
#include <Texture.hpp>
#include <Sprite.hpp>

///
/// Billboard -- A 3D textured quad that always face the camera.
/// It can also contain an animated sprite.
///
class Billboard
	: public ReferenceCountable
{
	friend class Renderer;

public:

	// Creates a new instance of the Billboard class.
	static Billboard * Create(const Sprite * anim, float distForAnimPlayback,
	                          float sizeFactor, const Vec3 & position);

	// Accessors:
	void SetSprite(const Sprite * s);
	const Sprite * GetSprite() const;
	void SetDistForAnimPlayback(float dist);
	float GetDistForAnimPlayback() const;

	// ReferenceCountable Methods:
	virtual unsigned long AddRef() const;
	virtual unsigned long Release() const;
	virtual unsigned long ReferenceCount() const;

protected:

	virtual ~Billboard();

private:

	// This will prevent the class from being staticaly allocated.
	// The only way to create an instance of it now is thru the Billboard::Create() method.
	Billboard(const Sprite * anim, float distForAnimPlayback,
	          float sizeFactor, const Vec3 & position);

	// Disable copy/assignment.
	Billboard(const Billboard &);
	Billboard & operator = (const Billboard &);

	const Sprite * sprite;
	GLuint textureID;
	float  distForAnimToPlay; // How far from the camera it must be to animate
	float  size;
	Vec3   pos;
};

///
/// Reference countable billboard pointer type.
///
typedef RefPtr<Billboard> BillboardPtr;

#endif // BILLBOARD_HPP
