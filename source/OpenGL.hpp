
// ===============================================================================================================
// -*- C++ -*-
//
// OpenGL.hpp - Gather all the OpenGL related stuff in one include file.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef MY_OPENGL_HPP
#define MY_OPENGL_HPP

#if defined (_WIN32) || defined (WIN32) || defined (_WIN64) || defined (WIN64)
#define	WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

// The Glew library for managing OpenGL extensions
#include <glew.h>

#ifdef _MSC_VER
#pragma comment (lib, "glew32.lib")
#endif // _MSC_VER

// Glut will include the other GL headers
#include <glut.h>

// =========================================================
// GL Utility Functions
// =========================================================

/// Initializes Glew and the OpenGL extensions.
/// Returns true if the platform has vertex/fragment shader support and vertex arrays support.
GLboolean glInitExtensions();

/// Begin a block of 2D drawing.
void glBegin2D(GLint windowWidth, GLint windowHeight);

/// End a 2D drawing block.
void glEnd2D();

/// Prints a format string to the window. Must be called between glBegin2D() and glEnd2D().
/// Use glColor() to set the text color.
GLint glPrintf(GLint x, GLint y, const char * format, ...);

#endif // MY_OPENGL_HPP
