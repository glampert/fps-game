
// ===============================================================================================================
// -*- C++ -*-
//
// Image.hpp - Declaration of an image interface class, a TGA image loader and an image factory.
//
// Copyright (c) 2006-2007 David Henry
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// Modified by: Guilherme R. Lampert - March 2011
// guilherme.ronaldo.lampert@gmail.com
//
// ===============================================================================================================

#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <Common.hpp>
#include <RefPtr.hpp>
#include <ReferenceCountable.hpp>
#include <MemoryBuffer.hpp>

// ===============================================================================================================
// Image class diagram:
//
//     +------- (abs)                +-------------------- (abs)
//     |  Image  | ----------------> |  ReferenceCountable  |
//     +---------+                   +----------------------+
//         ^
//         |
//         |                         +----------------+
//         |                         |  ImageFactory  | ---> Image objects can only be created
//         |   +------------+        +----------------+      by the ImageFactory.
//         +---|  ImageTGA  |
//             +------------+
//                                   +----------------+
//                                   |  MemoryBuffer  | ---> Image loading uses the MemoryBuffer class
//                                   +----------------+      internally.
//
// ===============================================================================================================

///
/// Image -- A generic image class for creating textures from.
/// All other specific image loaders are derived from it.
///
/// The Image class is reference counted and cloneable.
///
class Image
	: public ReferenceCountable
{
public:

	/// Tests if the image dimensions form a power of two image.
	bool IsPowerOfTwo() const;

	/// Checks if the image creation succeded.
	bool Fail() const;

	/// Returns a clone of the image.
	Image * Clone() const;

	// Accessors:
	int Width()  const { return width;  }
	int Height() const { return height; }
	int Format() const { return format; }
	int Components() const { return components; }
	int NumMipmaps() const { return numMipmaps; }
	bool StdCoordSystem() const { return standardCoordSystem; }
	const std::string & FileName() const { return fileName;   }

	/// Get a pointer to the image pixel buffer.
	/// Always call Release() on the return when done.
	MemoryBuffer * Pixels() const
	{
		pixels->AddRef();
		return pixels;
	}

	// ReferenceCountable Methods:
	virtual unsigned long AddRef() const;
	virtual unsigned long Release() const;
	virtual unsigned long ReferenceCount() const;

protected:

	// This will prevent the class from being statically allocated.
	// The only way to create an instance of it now is thru the ImageFactory class.

	Image() // Construct an invalid image.
		: width(0)
		, height(0)
		, numMipmaps(0)
		, format(0)
		, components(0)
		, pixels(0)
		, standardCoordSystem(true)
	{ }

	Image(const Image & rhs) // Construct from copy.
		: width(rhs.width)
		, height(rhs.height)
		, numMipmaps(rhs.numMipmaps)
		, format(rhs.format)
		, components(rhs.components)
		, pixels(rhs.pixels->Clone())
		, fileName(rhs.fileName)
		, standardCoordSystem(rhs.standardCoordSystem)
	{ }

	virtual ~Image();

protected:

	// Image size info:
	int width;
	int height;
	int numMipmaps;

	// Texture format and internal format (components):
	unsigned int format;
	int components;

	// Image data stored in a MemoryBuffer:
	MemoryBuffer * pixels;

	// File that originated the image.
	std::string fileName;

	// Is the picture in standard OpenGL 2D coordinate
	// system? (starts lower-left corner)
	bool standardCoordSystem;

private:

	// Disable assignment copy.
	Image & operator = (const Image &);
};

///
/// Reference countable image pointer type.
///
typedef RefPtr<Image> ImagePtr;

// ===============================================================================================================

///
/// ImageTGA -- A TrueVision TARGA (TGA) image loader class.
/// Support 24-32 bits BGR files; 16 bits RGB; 8 bits indexed (BGR palette);
/// 8 and 16 bits grayscale; all compressed and uncompressed.
/// Compressed TGA images use RLE algorithm.
///
class ImageTGA
	: public Image
{
	/*
	 * Copyright (c) 2006-2007 David Henry
	 *
	 * Modified by: Guilherme R. Lampert - March 2011
	 */

public:

	ImageTGA(const MemoryBuffer * memory, const std::string & originalFile);
	ImageTGA(const std::string & fileName);

private:

	///
	/// TGA Image Header.
	///
	#pragma pack (push, 1)
	struct TGA_Header
	{
		GLubyte id_lenght;        ///< Size of image id
		GLubyte colormap_type;    ///< 1 is has a colormap
		GLubyte image_type;       ///< Compression type

		short cm_first_entry;     ///< Colormap origin
		short cm_length;          ///< Colormap length
		GLubyte cm_size;          ///< Colormap size

		short x_origin;           ///< Bottom left x coord origin
		short y_origin;           ///< Bottom left y coord origin

		short width;              ///< Picture width (in pixels)
		short height;             ///< Picture height (in pixels)

		GLubyte pixel_depth;      ///< Bits per pixel: 8, 16, 24 or 32
		GLubyte image_descriptor; ///< 24 bits = 0x00; 32 bits = 0x80
	};
	#pragma pack (pop)

	/// Extract OpenGL texture informations from a TGA header.
	void getTextureInfo(const TGA_Header * header);

	/// Do the actual work reading the image.
	bool readTGAData(const MemoryBuffer * memory, const std::string & originalFile);

	// Uncompressed images:
	void readTGA8bits(const GLubyte * data, const GLubyte * colormap);
	void readTGA16bits(const GLubyte * data);
	void readTGA24bits(const GLubyte * data);
	void readTGA32bits(const GLubyte * data);
	void readTGAgray8bits(const GLubyte * data);
	void readTGAgray16bits(const GLubyte * data);

	// RLE compressed images:
	void readTGA8bitsRLE(const GLubyte * data, const GLubyte * colormap);
	void readTGA16bitsRLE(const GLubyte * data);
	void readTGA24bitsRLE(const GLubyte * data);
	void readTGA32bitsRLE(const GLubyte * data);
	void readTGAgray8bitsRLE(const GLubyte * data);
	void readTGAgray16bitsRLE(const GLubyte * data);

	// NOTE:
	// 16 bits images are stored in RGB
	// 8-24-32 images are stored in BGR(A)

	// RGBA/BGRA component table access -- usefull for switching from bgra to rgba at load time.
	static const int rgbaTable[4]; // bgra to rgba: 2, 1, 0, 3
	static const int bgraTable[4]; // bgra to bgra: 0, 1, 2, 3
};

// ===============================================================================================================

///
/// ImageFactory -- An image factory class.
///
/// This class is not instantiable, it has all its methods declared as static.
///
class ImageFactory
{
public:

	/// Loads an image from a file.
	static Image * CreateImageFromFile(const std::string & fileName);

	/// Create an image from a memory buffer containing the image data.
	/// It also expects a name (usually the name of the file that originated the image)
	/// that is used to identify the image by some other classes, such as textures.
	static Image * CreateImageFromMemory(const MemoryBuffer * memory, const std::string & originalFile);

private:

	ImageFactory();
	ImageFactory & operator = (const ImageFactory &);
};

#endif // IMAGE_HPP
